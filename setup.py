from setuptools import find_packages, setup

with open('version', 'r') as version_file:
    version = version_file.read().strip()

setup(
    name='test_project_1_2022-03-09_v1_dev',
    packages=find_packages(where='prod', exclude=['tests']),
    package_dir={'': 'prod'},
    version=version,
    description=' test project 1',
    author='po_1 po_1'
)
