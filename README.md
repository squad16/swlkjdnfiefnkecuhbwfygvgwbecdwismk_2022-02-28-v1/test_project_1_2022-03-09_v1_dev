test_project_1_2022-03-09_v1_dev
==============================

 test project 1

Project Organization
------------


```
├── airflow                                  <- A directory for launching project on airflow
│   ├── test_project_1_2022-03-09_v1_dev_dag.py  <- Dag file
│   └── run.py                               <- Exploitation scheduler (Exploitation runs this file,
│                                               which runs dag.py)
│
├── dev                                      <- A directory of DS development
│   ├── artifacts                            <- Artifacts created during learning (model, features,
│   │                                           hyperparameters selection)
│   ├── notebooks                            <- Jupyter notebooks (logging of MLFlow happens here).
│   │                                           Naming convention is a number (for ordering),
│   │                                           the creator's initials, and a short `-` delimited description, e.g.
│   │                                           `1.0-jqp-initial-data-exploration`.
│   └── sql                                  <- sql scripts with target event collecting
│
├── docs                                     <- A default Sphinx project; see sphinx-doc.org for details
│   ├── commands.rst
│   ├── conf.py
│   ├── getting-started.rst
│   └── index.rst
│
├── prod
│   │
│   ├── test_project_1_2022-03-09_v1_dev      <- Source code for use in this project.
│   │   ├── __init__.py
│   │   │
│   │   ├── data                             <- Data quality
│   │   │   ├── __init__.py
│   │   │   └── make_dataset.py
│   │   │
│   │   ├── scoring                          <- Scoring scripts
│   │   │   ├── __init__.py
│   │   │   ├── export_oracle.py 
│   │   │   └── scoring_script.py
│   │   │
│   │   ├── train                            <- Scripts to train models and then use trained models to make
│   │   │   │                                   predictions
│   │   │   ├── predict_model.py
│   │   │   └── train_model.py
│   │   │
│   │   └── validation                       <- Model Quality
│   │
│   └── tests                                <- Tests for scripts
├── __init__.py
├── conda.yml                                <- Environment build by MLFlow
├── config.yml                               <- Developer's config file
├── MANIFEST.in
├── MLProject                                <- File for launches generation (scripts from prod) on prod
├── requirements.txt                         <- The requirements file for reproducing the analysis environment,
│                                               generated with `pip freeze > requirements.txt`
├── setup.py                                 <- Makes project pip installable (pip install -e .) so project can be imported
├── tox.ini                                  <- Tox file with settings for running tox; see tox.readthedocs.io
└── version                                  <- Version of project's build
```

--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>
