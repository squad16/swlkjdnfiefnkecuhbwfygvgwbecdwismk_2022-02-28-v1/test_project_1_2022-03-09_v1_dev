import os
from datetime import timedelta

from airflow import DAG
from airflow.utils.dates import days_ago
from mf_airflow_plugin.providers.mlproject.operators.mlproject import MLProjectOperator

# dag version $VERSION

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': days_ago(2),
    'email': ['po_1 po_1'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

env = os.environ.get('EXECUTION_ENVIRONMENT', 'PREPROD')

if env not in {'PREPROD', 'PROD'}:
    raise ValueError(f'Unsupported environment {env}')

dag = DAG(
    dag_id='test_project_1_2022-03-09_v1_dev',
    default_args=default_args,
    schedule_interval=None,
    is_paused_upon_creation=False
)

scoring = MLProjectOperator(
    task_id='scoring',
    dag=dag,
    project_name="test_project_1_2022-03-09_v1_dev",
    project_version='$VERSION',
    entry_point='score',
    base_image='docker-itbigdata.megafon.ru/mlops/mlflow_project_runner',
    base_image_version='0.1.37',
    hadoop_env=env,
    project_parameters={
        'snap_date': "{{dag_run.conf['snap_date']}}",
    },
    environment={
        'MLFLOW_S3_ENDPOINT_URL': '{{var.value.MLFLOW_S3_ENDPOINT_URL}}',
        'AWS_ACCESS_KEY_ID': '{{var.value.MLFLOW_ACCESS_KEY_ID}}',
        'AWS_SECRET_ACCESS_KEY': '{{var.value.MLFLOW_ACCESS_KEY_SECRET}}',
    }
)

export = MLProjectOperator(
    task_id='export',
    dag=dag,
    project_name="test_project_1_2022-03-09_v1_dev",
    project_version='$VERSION',
    entry_point='export_oracle',
    base_image='docker-itbigdata.megafon.ru/mlops/mlflow_project_runner',
    base_image_version='0.1.37',
    hadoop_env=env,
    environment={
        'snap_date': "{{dag_run.conf['snap_date']}}",
        'username': "{{var.value.DWX_DATABASE_USERNAME}}",
        'password': "{{var.value.DWX_DATABASE_PASSWORD_SECRET}}",
        'jdbc': "{{var.value.DWX_JDBC_CONNECTION_STRING}}",
    }
)

scoring >> export
